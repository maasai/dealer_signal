<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listings', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('make_id')->references('id')->on('makes');
            $table->integer('model_id')->references('id')->on('models');
            $table->integer('category_id')->references('id')->on('categories');
            $table->integer('condition_id')->references('id')->on('conditions');
            $table->integer('fuel_id')->references('id')->on('fuels');
            $table->integer('transmission_id')->references('id')->on('transmissions');
            $table->integer('color_id')->references('id')->on('colors');
            $table->integer('registration_year');
            $table->integer('mileage');
            $table->double('price');
            $table->boolean('featured')->default(0);
            $table->boolean('active')->default(1);
            $table->string('extra_details');
            $table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listings');
	}

}
