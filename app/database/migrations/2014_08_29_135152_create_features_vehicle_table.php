<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesVehicleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('features_vehicle', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('listing_id')->references('id')->on('listings');
            $table->integer('feature_id')->references('id')->on('features');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('features_vehicle');
	}

}
