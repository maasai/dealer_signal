<?php

class OAuthScopeSeeder extends Seeder {

    public function run()
    {
        DB::table('oauth_scopes')->insert([
            'scope' => 'basic',
            'name' => 'Basic scope',
            'description' => 'Basic scope',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
    }

} 