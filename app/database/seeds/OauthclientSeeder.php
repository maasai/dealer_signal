<?php

class OAuthClientSeeder extends Seeder {

    public function run()
    {
        DB::table('oauth_clients')->insert([
            'id' => str_random(8),
            'secret' => str_random(12),
            'name' => 'Test Client #1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
    }

} 