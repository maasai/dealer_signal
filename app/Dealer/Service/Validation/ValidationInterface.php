<?php  namespace Dealer\Service\Validation;


interface ValidationInterface {

    /**
     * Add data to validate against
     * @param array $input
     * @return mixed Dealer\Service\Validation\ValidationInterface
     */
    public function with(array $input);

    /**
     * Test if validation passes
     * @return boolean
     */
    public function passes();

    /**
     *Retrieve validation errors
     * @return mixed
     */
    public function errors();

} 