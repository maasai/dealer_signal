<?php  namespace Dealer\Service\Form\Listing;


use Dealer\Repositories\Listing\ListingRepository;
use Dealer\Service\Validation\ValidationInterface;

class ListingForm {

    protected $data;

    protected $validator;

    protected $listing;

    public function __construct(ValidationInterface $validator, ListingRepository $listing){

        $this->validator = $validator;
        $this->listing = $listing;
    }

    public function save(array $input){
        if( ! $this->valid($input) )
        {
            return false;
        }

        return $this->listing->create($input);

     }

    public function update(array $input){
        if( ! $this->valid($input) )
        {
            return false;
        }

        return $this->listing->update($input);

    }

    public function delete($id)
    {
        return $this->listing->delete($id);
    }

    public function errors(){
        return $this->validator->errors();
    }

    public function valid(array $input){
        return $this->validator->with($input)->passes();
    }

} 