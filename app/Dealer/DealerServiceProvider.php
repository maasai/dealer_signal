<?php  namespace Dealer;

use Dealer\Service\Form\Listing\ListingForm;
use Dealer\Service\Form\Listing\ListingValidator;
use Illuminate\Support\ServiceProvider;

class DealerServiceProvider extends ServiceProvider{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Dealer\Service\Form\Listing\ListingForm', function($app)
             {
                    return new ListingForm(
                       new ListingValidator( $app['validator'] ),
                        $app->make('Dealer\Repositories\Listing\ListingRepository')
                     );
             }
        );

        $this->app->bind(
            'Dealer\Repositories\Category\CategoryRepository',
            'Dealer\Repositories\Category\EloquentCategoryRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Color\ColorRepository',
            'Dealer\Repositories\Color\EloquentColorRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Condition\ConditionRepository',
            'Dealer\Repositories\Condition\EloquentConditionRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Feature\FeatureRepository',
            'Dealer\Repositories\Feature\EloquentFeatureRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Listing\ListingRepository',
            'Dealer\Repositories\Listing\EloquentListingRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Make\MakeRepository',
            'Dealer\Repositories\Make\EloquentMakeRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Model\ModelRepository',
            'Dealer\Repositories\Model\EloquentModelRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\User\UserRepository',
            'Dealer\Repositories\User\EloquentUserRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Page\PageRepository',
            'Dealer\Repositories\Page\EloquentPageRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Transmission\TransmissionRepository',
            'Dealer\Repositories\Transmission\EloquentTransmissionRepository'
        );

        $this->app->bind(
            'Dealer\Repositories\Fuel\FuelRepository',
            'Dealer\Repositories\Fuel\EloquentFuelRepository'
        );
    }

}