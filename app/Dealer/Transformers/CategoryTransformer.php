<?php  namespace Dealer\Transformers;


class CategoryTransformer extends BaseTransformer {

    public function transform($category)
    {
        return [
            'id'            => $category['id'],
            'category_name' => $category['category_name'],
           ];
    }

} 