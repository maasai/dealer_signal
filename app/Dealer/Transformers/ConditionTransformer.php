<?php  namespace Dealer\Transformers;

class ConditionTransformer extends BaseTransformer {

    public function transform($condition)
    {
        return [
            'id'                => $condition['id'],
            'condition_name'    => $condition['condition_name'],
        ];
    }

}