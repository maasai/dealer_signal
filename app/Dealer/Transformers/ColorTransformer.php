<?php  namespace Dealer\Transformers;


class ColorTransformer extends BaseTransformer {

    public function transform($color)
    {
        return [
            'id'            => $color['id'],
            'color_name'    => $color['color_name'],
        ];
    }

} 