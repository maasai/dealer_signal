<?php  namespace Dealer\Transformers;


class UserTransformer extends BaseTransformer {

    public function transform($user)
    {
        return [
            'id'            => $user['id'],
            'first_name'    => $user['first_name'],
            'last_name'     => $user['last_name'],
            'email'         => $user['email'],
            'activated'     => $user['activated'],
            'deleted_at'    => $user['deleted_at'],
            'created_at'    => $user['created_at'],
            'updated_at'    => $user['updated_at']
        ];
    }

}