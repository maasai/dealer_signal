<?php  namespace Dealer\Transformers;

class ListingTransformer extends BaseTransformer {

    private $modelTransformer;
    private $featureTransformer;
    private $categoryTransformer;
    private $fuelTransformer;
    private $conditionTransformer;
    private $transmissionTransformer;
    private $colorTransformer;

    function __construct(FuelTransformer $fuelTransformer, ConditionTransformer $conditionTransformer, TransmissionTransformer $transmissionTransformer, ColorTransformer $colorTransformer, CategoryTransformer $categoryTransformer, ModelTransformer $modelTransformer, FeatureTransformer $featureTransformer)
    {
        $this->modelTransformer = $modelTransformer;
        $this->featureTransformer = $featureTransformer;
        $this->categoryTransformer = $categoryTransformer;
        $this->fuelTransformer = $fuelTransformer;
        $this->conditionTransformer = $conditionTransformer;
        $this->transmissionTransformer = $transmissionTransformer;
        $this->colorTransformer = $colorTransformer;
    }

    public function transform($listing)
    {
        return [
            'id'                => $listing['id'],
            'active'            => (bool)$listing['active'],
            'featured'          => (bool)$listing['featured'],
            'registration_year' => $listing['registration_year'],
            'price'             => $listing['price'],
            'mileage'           => $listing['mileage'],
            'fuel'             => $this->fuelTransformer->transform($listing['fuel']),
            'condition'        => $this->conditionTransformer->transform($listing['condition']),
            'transmission'     => $this->transmissionTransformer->transform($listing['transmission']),
            'color'            => $this->colorTransformer->transform($listing['color']),
            'category'          => $this->categoryTransformer->transform($listing['category']),
            'model'             => $this->modelTransformer->transform($listing['model']),
            'features'          => $this->featureTransformer->transform($listing['features']),
            'extra_details'     => $listing['extra_details'],
            'updated_at'        => $listing['updated_at']
        ];
    }

}