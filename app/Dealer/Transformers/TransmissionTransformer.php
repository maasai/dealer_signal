<?php  namespace Dealer\Transformers;


class TransmissionTransformer extends BaseTransformer {

    public function transform($transmission)
    {
        return [
            'id'                    => $transmission['id'],
            'transmission_name'     => $transmission['transmission_name'],
         ];
    }

}