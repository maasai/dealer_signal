<?php  namespace Dealer\Transformers;


class FuelTransformer extends BaseTransformer {

    public function transform($fuel)
    {
        return [
            'id'            => $fuel['id'],
            'fuel_name'     => $fuel['fuel_name'],
          ];
    }

}