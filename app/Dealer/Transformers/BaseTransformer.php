<?php  namespace Dealer\Transformers;


abstract class BaseTransformer
{

    /**
     * Implement to hide the db table field names
     * @param $item
     * @return mixed
     */
    public abstract function transform($item);


    /**
     * Transform a collection of provided items
     * @param array $items
     * @return array
     */
    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

}
