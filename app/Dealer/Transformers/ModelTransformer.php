<?php  namespace Dealer\Transformers;


class ModelTransformer extends BaseTransformer {

    private $makeTransformer;

    function __construct(MakeTransformer $makeTransformer)
    {
        $this->makeTransformer = $makeTransformer;
    }

    public function transform($model)
    {
        return [
            'id'            => $model['id'],
            'model_name'    => $model['model_name'],
            'make'          => $this->makeTransformer->transform($model['make'])
            ];
    }

}