<?php  namespace Dealer\Transformers;


class MakeTransformer extends BaseTransformer {

    public function transform($make)
    {
        return [
            'id'            => $make['id'],
            'make_name'     => $make['make_name']
        ];
    }

}