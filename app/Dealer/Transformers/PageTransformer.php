<?php  namespace Dealer\Transformers;


class PageTransformer extends BaseTransformer {

    public function transform($page)
    {
        return [
            'id'            => $page['id'],
            'page_name'     => $page['page_name'],
            'deleted_at'    => $page['deleted_at'],
            'created_at'    => $page['created_at'],
            'updated_at'    => $page['updated_at']
        ];
    }

} 