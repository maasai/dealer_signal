<?php  namespace Dealer\Repositories\Listing;


interface ListingRepository
{
    /**
     * Fetch all listings
     * @return mixed
     */
    function getAll();

    /**
     * Get a listing by ID
     * @param $id
     * @return mixed
     */
    function getByID($id);

    /**
     * Create a new listing
     * @param array $data
     * @return boolean
     */
    public function create(array $data);

    /**
     * Update an existing Listing
     * @param array $data data to update
     * @return boolean
     */
    public function update(array $data);

    /**
     * Delete an existing listing
     * @param $id
     * @return mixed
     */
    public function delete($id);

}