<?php  namespace Dealer\Repositories\Listing;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentListingRepository extends AbstractEloquentRepository implements ListingRepository
{
    protected $model;

    /**
     * Attach the model
     * @param \Listing $model
     */
    function __construct(\Listing $model)
    {
        $this->model = $model;
    }

    /**
     * Create a new listing
     * @param array $data
     * @return bool
     */
    public function create(array $data)
    {
        $features = isset($data['features'])?$data['features']:array();

        $featured = isset($data['featured'])?1:0;
        $active = isset($data['active'])?1:0;

        $listing = $this->model->create(array(
            'registration_year' => $data['registration_year'],
            'make_id' =>  $data['make_id'],
            'model_id' =>  $data['model_id'],
            'category_id' =>  $data['category_id'],
            'condition_id' =>  $data['condition_id'],
            'fuel_id' =>  $data['fuel_id'],
            'transmission_id' =>  $data['transmission_id'],
            'color_id' =>  $data['color_id'],
            'mileage' =>  $data['mileage'],
            'price' =>  $data['price'],
            'featured' =>  $featured,
            'active' =>  $active
        ));

        if (!$listing) {
            return false;
        }

        $this->model->find($listing->id)->features()->sync($features);
        return true;
    }

    /**
     * Update an existing listing
     * @param array $data
     * @return bool
     */
    public function update(array $data)
    {
        $listing = $this->model->find($data['id']);

        if(!$listing){
            return false;
        }

        $listing->registration_year = $data['registration_year'];
        $listing->price =   $data['price'];

        $listing->save();

        return true;

    }

    /** Delete an existing listing
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $listing  = $this->model->find($id);
        if(is_null($listing)){
            return false;
        }else{
            //destroy images
            //remove the features from relation
            $this->model->find($id)->features()->detach();
            //delete the listing
            $listing->destroy($id);

            return true;
        }

    }
}