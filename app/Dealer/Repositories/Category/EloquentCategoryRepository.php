<?php  namespace Dealer\Repositories\Category;
use Dealer\Repositories\AbstractEloquentRepository;


class EloquentCategoryRepository extends AbstractEloquentRepository implements CategoryRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Category $model
     */
    function __construct(\Category $model)
    {
        $this->model = $model;
    }
}

