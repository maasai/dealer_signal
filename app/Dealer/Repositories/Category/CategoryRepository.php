<?php  namespace Dealer\Repositories\Category;


interface CategoryRepository {

    function getAll();
    function getByID($id);

} 