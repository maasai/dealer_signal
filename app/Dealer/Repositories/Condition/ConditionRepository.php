<?php  namespace Dealer\Repositories\Condition;


interface ConditionRepository {

    function getAll();
    function getByID($id);

}