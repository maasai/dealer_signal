<?php  namespace Dealer\Repositories\Condition;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentConditionRepository extends AbstractEloquentRepository implements ConditionRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Condition $model
     */
    function __construct(\Condition $model)
    {
        $this->model = $model;
    }
}