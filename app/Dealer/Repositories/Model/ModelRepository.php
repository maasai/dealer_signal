<?php  namespace Dealer\Repositories\Model;

interface ModelRepository {

    function getAll();
    function getByID($id);

}