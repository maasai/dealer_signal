<?php  namespace Dealer\Repositories\Model;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentModelRepository extends AbstractEloquentRepository implements ModelRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Model $model
     */
    function __construct(\Model $model)
    {
        $this->model = $model;
    }
}