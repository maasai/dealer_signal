<?php  namespace Dealer\Repositories\Fuel;


interface FuelRepository {

    function getAll();
    function getByID($id);

}