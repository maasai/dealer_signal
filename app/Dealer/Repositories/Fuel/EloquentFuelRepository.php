<?php  namespace Dealer\Repositories\Fuel;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentFuelRepository extends AbstractEloquentRepository implements FuelRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Fuel $model
     */
    function __construct(\Fuel $model)
    {
        $this->model = $model;
    }
}
