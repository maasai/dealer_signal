<?php  namespace Dealer\Repositories\User;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentUserRepository extends AbstractEloquentRepository implements UserRepository {
    protected $model;

    /**
     * Attach the model
     * @param \User $model
     */
    function __construct(\User $model)
    {
        $this->model = $model;
    }
}
