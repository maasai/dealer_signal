<?php  namespace Dealer\Repositories\User;


interface UserRepository {

    function getAll();
    function getByID($id);

}