<?php  namespace Dealer\Repositories\Page;


interface PageRepository {

    function getAll();
    function getByID($id);

}