<?php  namespace Dealer\Repositories\Page;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentPageRepository extends AbstractEloquentRepository implements PageRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Page $model
     */
    function __construct(\Page $model)
    {
        $this->model = $model;
    }
}
