<?php  namespace Dealer\Repositories\Feature;

use Dealer\Repositories\AbstractEloquentRepository;
use Illuminate\Support\Facades\Input;


class EloquentFeatureRepository extends AbstractEloquentRepository implements FeatureRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Feature $model
     */
    function __construct(\Feature $model)
    {
        $this->model = $model;
    }

    public function getAll($load = array(), $paginate = true)
    {
        $limit = Input::get('limit') ?: 10;
        $data = $this->model->with($load)->paginate($limit);
        if(false == $paginate)
        {
            $data = $this->model->with($load)->orderBy('feature_name', 'ASC');
        }
        return $data;
    }
}