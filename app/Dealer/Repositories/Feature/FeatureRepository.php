<?php  namespace Dealer\Repositories\Feature;


interface FeatureRepository {

    function getAll();
    function getByID($id);

}