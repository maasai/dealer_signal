<?php  namespace Dealer\Repositories\Transmission;


interface TransmissionRepository {

    function getAll();
    function getByID($id);

}