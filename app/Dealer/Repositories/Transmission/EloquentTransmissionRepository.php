<?php  namespace Dealer\Repositories\Transmission;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentTransmissionRepository extends AbstractEloquentRepository implements TransmissionRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Transmission $model
     */
    function __construct(\Transmission $model)
    {
        $this->model = $model;
    }
}
