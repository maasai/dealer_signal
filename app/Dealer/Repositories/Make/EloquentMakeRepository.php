<?php  namespace Dealer\Repositories\Make;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentMakeRepository extends AbstractEloquentRepository implements MakeRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Make $model
     */
    function __construct(\Make $model)
    {
        $this->model = $model;
    }
}