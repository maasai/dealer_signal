<?php  namespace Dealer\Repositories\Make;


interface MakeRepository {

    function getAll();
    function getByID($id);

}