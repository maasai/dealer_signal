<?php  namespace Dealer\Repositories\Color;

use Dealer\Repositories\AbstractEloquentRepository;


class EloquentColorRepository extends AbstractEloquentRepository implements ColorRepository {
    protected $model;

    /**
     * Attach the model
     * @param \Color $model
     */
    function __construct(\Color $model)
    {
        $this->model = $model;
    }
}
