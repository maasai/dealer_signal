<?php  namespace Dealer\Repositories\Color;


interface ColorRepository {

    function getAll();
    function getByID($id);

}