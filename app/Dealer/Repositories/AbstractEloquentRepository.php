<?php  namespace Dealer\Repositories;

use Illuminate\Support\Facades\Input;

abstract class AbstractEloquentRepository {

    /**
     * @param array $load
     * @param bool $paginate
     * @return mixed
     */

    public function getAll($load = array(), $paginate = true)
    {
        $limit = Input::get('limit') ?: 10;
        $data = $this->model->with($load)->paginate($limit);
            if(false == $paginate)
            {
                $data = $this->model->with($load);
            }
        return $data;
    }

    /**
     * Fetch a single item from db table. Limit to filters if provided
     * @param $id
     * @param array $filters
     * @param array $load
     * @return mixed
     */
    public function getById($id, $filters = array(), $load = array())
    {
        if(!empty($filters))
        {
            return $this->model->find($id, $filters);
        }

        return $this->model->find($id)->with($load)->first();

    }

    public function save($data)
    {
        return $this->model->create($data);
    }

    public function destroy()
    {
        return $this->model->destroy();
    }


    public function create(array $attributes)
    {
        return call_user_func_array("{$this->model}::create", array($attributes));
    }

}