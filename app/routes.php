<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});


Route::get('login', 'UsersController@getLogin');
Route::get('logout', 'UsersController@getLogout');
Route::post('login', 'UsersController@postLogin');

Route::post('oauth/access_token', function()
{
    return AuthorizationServer::performAccessTokenFlow();
});

/**
 * These routes need a logged in user
 */
Route::group(array('before' => 'auth'), function(){
    Route::Resource('colors', 'ColorsController');
    Route::Resource('makes', 'MakesController');
    Route::Resource('models', 'ModelsController');
    Route::Resource('pages', 'PagesController');
    Route::Resource('transmissions', 'TransmissionsController');
    Route::Resource('fuels', 'FuelsController');
    Route::Resource('features', 'FeaturesController');
    Route::Resource('categories', 'CategoriesController');
    Route::Resource('conditions', 'ConditionsController');
    Route::Resource('users', 'UsersController');
    Route::Resource('listings', 'ListingsController');
});

//Grouped api routes. The API is read only - collection and single for each entity

Route::group(array('prefix' => 'api/v1'), function()
{
    Route::Resource('colors', 'ColorsController', ['only' => ['index', 'show']]);
    Route::Resource('makes', 'MakesController', ['only' => ['index', 'show']]);
    Route::Resource('models', 'ModelsController', ['only' => ['index', 'show']]);
    Route::Resource('pages', 'PagesController', ['only' => ['index', 'show']]);
    Route::Resource('transmissions', 'TransmissionsController', ['only' => ['index', 'show']]);
    Route::Resource('fuels', 'FuelsController', ['only' => ['index', 'show']]);
    Route::Resource('features', 'FeaturesController', ['only' => ['index', 'show']]);
    Route::Resource('categories', 'CategoriesController', ['only' => ['index', 'show']]);
    Route::Resource('conditions', 'ConditionsController', ['only' => ['index', 'show']]);
    Route::Resource('listings', 'ListingsController', ['only' => ['index', 'show']]);
});