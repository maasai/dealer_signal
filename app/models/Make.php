<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Make extends \Eloquent {
	use SoftDeletingTrait;

	protected $dates = array('created_at', 'updated_at', 'deleted_at');
	protected $fillable = [];

	public static $rules = array(
		'make_name' => 'required|min:1|unique:makes,make_name,NULL,id,deleted_at,NULL'
		);

	public function models(){
		return $this->hasMany('Model');
	}

    public function listings(){
        return $this->hasMany('Listing');
    }
}