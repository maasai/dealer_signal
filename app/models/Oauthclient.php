<?php
class Oauthclient extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_clients';

    protected $fillable = ['secret', 'name'];

}