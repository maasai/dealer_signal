<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Fuel extends \Eloquent {
	use SoftDeletingTrait;

	protected $dates = array('created_at', 'updated_at', 'deleted_at');
	protected $fillable = [];

    public function listings(){
        return $this->hasMany('Listing');
    }

	public static $rules = array(
    'fuel_name' => 'required|min:1|unique:fuels,fuel_name,NULL,id,deleted_at,NULL'
    );
}