<?php
class Oauthscope extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_scopes';

    protected $fillable = ['name', 'scope', 'description'];


} 