<?php

class Feature extends \Eloquent {

	protected $dates = array();
	protected $fillable = [];

    public $timestamps = false;

	public static $rules = array(
		'feature_name' => 'required|min:1|unique:features,feature_name,NULL,id'
		);

    public function listings(){
        return $this->belongsToMany('Listing', 'features_vehicle');
    }
}