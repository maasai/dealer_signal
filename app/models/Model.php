<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Model extends \Eloquent {
	use SoftDeletingTrait;

	protected $dates = array('created_at', 'updated_at', 'deleted_at');
	protected $fillable = [];

	public static $rules = array(
		'model_name' => 'required|min:1|unique:models,model_name,NULL,id,deleted_at,NULL'
		);

	public function make(){
		return $this->belongsTo('make');
	}

    public function listings(){
        return $this->hasMany('Listing');
    }
}