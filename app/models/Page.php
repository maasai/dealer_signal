<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Page extends \Eloquent {
	use SoftDeletingTrait;

	protected $dates = array('created_at', 'updated_at', 'deleted_at');
	protected $fillable = [];

	public static $rules = array(
		'page_name' => 'required|min:1|unique:pages,page_name,NULL,id,deleted_at,NULL',
		'page_content' => 'required'
		);
}