<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Listing extends \Eloquent {
	use SoftDeletingTrait;

	protected $dates = array('created_at', 'updated_at', 'deleted_at');
	protected $fillable = [
        'registration_year',
        'make_id',
        'model_id',
        'condition_id',
        'category_id',
        'fuel_id',
        'transmission_id',
        'color_id',
        'mileage',
        'price',
        'featured',
        'active'
    ];

    /**
     * Features Relationship
     * @return mixed
     */
    public function features(){
        return $this->belongsToMany('Feature', 'features_vehicle');
    }

    /**
     * Make Relationship
     * @return mixed
     */
    public function make(){
        return $this->belongsTo('Make');
    }

    /**
     * Model Relationship
     * @return mixed
     */
    public function model(){
        return $this->belongsTo('Model');
    }

    /**
     * Fuel Relationship
     * @return mixed
     */
    public function fuel(){
        return $this->belongsTo('Fuel');
    }

    /**
     * Condition Relationship
     * @return mixed
     */
    public function condition(){
        return $this->belongsTo('Condition');
    }

    /**
     * Transmission Relationship
     * @return mixed
     */
    public function transmission(){
        return $this->belongsTo('Transmission');
    }

    /**
     * Color Relationship
     * @return mixed
     */
    public function color(){
        return $this->belongsTo('Color');
    }

    /**
     * Category Relationship
     * @return mixed
     */
    public function category()
    {
        return $this->belongsTo('Category');
    }


   }