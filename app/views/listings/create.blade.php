@extends('layouts.main')
@section('content')



<!-- Main content -->
<section class="content">

    <div class='row'>
        <div class='col-md-12'>
            @if($errors->any())
            <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                {{ implode('', $errors->all(':message<br/>')) }}
            </div>
            @endif
            <div class='box'>
                <div class='box-body pad'>
                    <!--page data here-->

                    {{ Form::open(array('route'=>'listings.store')) }}
                <div class='row'>
                    <div class="form-group col-md-4">
                        {{ Form::label('category', 'Category') }}
                        {{ Form::select('category_id', $categories, '', array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group col-md-4">
                        {{ Form::label('make', 'Make') }}
                        {{ Form::select('make_id', $makes, '', array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group col-md-4">
                        {{ Form::label('model', 'Model') }}
                        {{ Form::select('model_id', $models, '', array('class'=>'form-control')) }}
                    </div>
                </div>
                    <div class='row'>
                        <div class="form-group col-md-4">
                            {{ Form::label('condition', 'Condition') }}
                            {{ Form::select('condition_id', $conditions, '', array('class'=>'form-control')) }}
                        </div>

                        <div class="form-group col-md-4">
                            {{ Form::label('year', 'Year') }}
                            {{ Form::text('registration_year', Input::old('year'), array('placeholder'=>'Year', 'class'=>'form-control')) }}
                        </div>

                        <div class="form-group col-md-4">
                            {{ Form::label('mileage', 'Mileage') }}
                            {{ Form::text('mileage', Input::old('mileage'), array('placeholder'=>'Mileage', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-4">
                            {{ Form::label('fuel', 'Fuel Type') }}
                            {{ Form::select('fuel_id', $fuels, '', array('class'=>'form-control')) }}
                        </div>

                        <div class="form-group col-md-4">
                            {{ Form::label('transmission', 'Transmission') }}
                            {{ Form::select('transmission_id', $transmissions, '', array('class'=>'form-control')) }}
                        </div>

                        <div class="form-group col-md-4">
                            {{ Form::label('color', 'Color (Exterior)') }}
                            {{ Form::select('color_id', $colors, '', array('class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-4">
                            {{ Form::label('price', 'Sale Price') }}
                            {{ Form::text('price', Input::old('price'), array('placeholder'=>'Sale Price', 'class'=>'form-control')) }}
                        </div>

                        <div class="form-group col-md-4">
                            <br/>

                            {{ Form::checkbox('featured', 1, '', array('id' => 'featured'))}}
                            {{ Form::label('featured', 'Vehicle is featured')}}

                        </div>

                        <div class="form-group col-md-4">
                            <br/>
                            {{ Form::checkbox('active', 1, true, array('id' => 'active')) }}
                            {{ Form::label('active', 'Vehicle is active')}}

                        </div>
                    </div>

                    <div class='box'>
                        <div class='box-body pad'>
                            <div class="row">
                                <div class="form-group col-md-10">
                                    <h4>Extra Features</h4>
                                </div>
                                <div class="form-group">
                                    <h5>{{ Form::checkbox('', '', '', array('id' => 'all')) }}
                                        {{ Form::label('all', 'Check / Uncheck All')}}
                                    </h5>
                                </div>
                            </div>

                            <div class="row">

                                @foreach($features as $id => $feature)
                                <div class="form-group col-md-2">
                                    {{Form::checkbox('features[]', $id, '', array('id' => $feature))}}
                                    {{ Form::label($feature, $feature)}}
                                </div>
                                @endforeach

                            </div>


                            <!--end custom page data here-->
                        </div>
                    </div><!-- /.box -->

                </div>



            </div>




                <div class='box-body pad'>

                        <div class="row col-lg-11">
                            <div class="form-group">
                                <a href="{{ URL::to('listings') }}" class="btn btn-default btn-cons pull-left"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>
                                {{ Form::submit('Save', array('class'=>'btn btn-primary pull-right')) }}
                                {{ Form::close() }}

                            </div>
                        </div>

                </div>




        </div><!-- /.col-->
    </div><!-- ./row -->



</section><!-- /.content -->
</aside><!-- /.right-side -->
@stop