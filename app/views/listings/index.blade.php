@extends('layouts.main')
@section('content')



<!-- Main content -->
<section class="content">

    <div class='row'>
        <div class='col-md-12'>
            <div class='box'>
                <div class='box-body pad table-responsive'>
                    <!--page data here-->
                    <a href="{{ URL::to('listings/create') }}" class="btn btn-success pull-right"><i class="fa fa-plus-square"></i> New listing</a>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Extra Features</th>
                            <th>date added</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $count = $listings->getFrom(); ?>
                        @foreach ($listings as $listing)
                        <tr>
                            <td>{{ $count }}</td>
                            <td>image</td>
                            <td class="col-md-2">
                                Year:&nbsp;&nbsp;{{ $listing->registration_year }}<br>
                                Make:&nbsp;&nbsp;{{ $listing->make['make_name'] }}<br>
                                Model:&nbsp;&nbsp;{{ $listing->model['model_name'] }}<br>
                                Price:&nbsp;&nbsp;{{ $listing->price }}<br>

                            </td>
                            <td class="col-md-2">
                                <select multiple="" class="form-control" disabled="">
                            @foreach($listing->features as $feature)
                                    <option>{{$feature['feature_name']}}</option>
                            @endforeach
                                </select>
                            </td>
                            <td>date</td>

                            <td class="no-print">
                                <a href="{{ URL::to('listings/'.$listing->id.'/edit') }}" class="btn btn-info pull-right btn-sm"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                            <td class="no-print">
                                {{ Form::open(array('method'=> 'DELETE', 'route' => array('listings.destroy', $listing->id))) }}
                                {{Form::button('<i class="fa fa-times"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick'=>'if(!confirm("Are you sure to delete this item? This cannot be undone.")){return false;}'))}}
                                {{ Form::close() }}
                            </td>

                        </tr>
                        <?php $count++; ?>
                        @endforeach
                        </tbody>
                    </table>


                    <!--end custom page data here-->
                </div>
            </div><!-- /.box -->


        </div><!-- /.col-->
    </div><!-- ./row -->

    <div class="row no-print">
        {{$listings->links()}}
        <div class="col-xs-12">
            <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
            <a href="{{ URL::to('listings/create') }}" class="btn btn-success pull-right"><i class="fa fa-plus-square"></i> New listing</a>

        </div>
    </div>

</section><!-- /.content -->
</aside><!-- /.right-side -->
@stop