@extends('layouts.main')
@section('content')

          

                <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-6'>
                            @if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <div class='box'>
                                   <div class='box-body pad'>
                                    <!--page data here-->
                               
         {{ Form::open(array('route'=>'users.store')) }}
    
        <div class="form-group">                
            {{ Form::label('first_name', 'First Name') }}
            {{ Form::text('first_name', '', array('placeholder'=>'User First Name', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group">               
            {{ Form::label('last_name', 'Last Name') }}
            {{ Form::text('last_name', '', array('placeholder'=>'User Last Name', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group">               
            {{ Form::label('email', 'Email Address') }}
            {{ Form::text('email', '', array('placeholder'=>'User Email Address', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group">               
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control')) }}
        </div>
           <div class="form-group">               
            {{ Form::label('password_confirmation', 'Confirm Password') }}
            {{ Form::password('password_confirmation',array('placeholder'=>'Confirm Password', 'class'=>'form-control')) }}
        </div>
        <div class="form-group">                
          {{ Form::submit('Save User', array('class'=>'btn btn-primary')) }}  
        </div>

  

                                    <!--end custom page data here-->
                                </div>
                            </div><!-- /.box -->

                          
                        </div><!-- /.col-->
                    </div><!-- ./row -->



                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop