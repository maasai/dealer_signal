<!DOCTYPE html>
<html>
<head>
    @include('components.head')
</head>
<body class="skin-blue">

        <div class="form-box" id="login-box">

            <div class="header">Dealer Signal</div>
                {{ Form::open(array('url'=>'login')) }}
                <div class="body bg-gray">
                    <div>
                        @if(Session::has('message'))
                        <h5  class="alert alert-danger">{{ Session::get('message') }}</h5>
                        @endif

                        @if($errors->any())
                        <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                            {{ implode('', $errors->all(':message<br/>')) }}
                        </div>
                        @endif

                    </div>
                    <div class="form-group">
                        {{Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Admin Email'))}}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                    </div>

                    <div class="form-group">
                        <h5>{{ Form::checkbox('remember_me', '', '', array('id' => 'remember_me')) }}
                            {{ Form::label('remember_me', 'Remember me')}}
                        </h5>
                    </div>
                </div>
                <div class="footer">
                    {{Form::submit('Admin Login', array('class' => 'btn bg-olive btn-block'))}}

                    <p><a href="#">I forgot my password</a></p>
                </div>
            {{ Form::close() }}


        </div>


        <!-- Page content end-->

<!-- add new calendar event modal -->

</body>
</html>
 