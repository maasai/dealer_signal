@extends('layouts.main')
@section('content')

<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-6">
     @if($errors->any())
<div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
{{ implode('', $errors->all(':message<br/>')) }}
</div>
        @endif
    <!-- general form elements -->
    <div class="box box-primary">
                               
                              <!-- form start -->
        {{ Form::model($user, array('method' => 'PATCH', 'route' => array('users.update', $user->id)))}}

    <div class="box-body">
        <div class="form-group">              	
	        {{ Form::label('first_name', 'First Name') }}
	        {{ Form::text('first_name', Input::old('first_name'), array('placeholder'=>'User First Name', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group">              	
	        {{ Form::label('last_name', 'Last Name') }}
	        {{ Form::text('last_name', Input::old('last_name'), array('placeholder'=>'User Last Name', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group">              	
	        {{ Form::label('email', 'Email Address') }}
	        {{ Form::text('email', Input::old('email'), array('placeholder'=>'User Email Address', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group">               
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control')) }}
        </div>
           <div class="form-group">               
            {{ Form::label('password_confirmation', 'Confirm Password') }}
            {{ Form::password('password_confirmation',array('placeholder'=>'Confirm Password', 'class'=>'form-control')) }}
        </div>
        <div class="form-group">              	
	      {{ Form::submit('Save User', array('class'=>'btn btn-primary')) }}  
        </div>

    </div>       
                            </div><!-- /.box -->

               
                        </div><!--/.col (left) -->
            
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop