@extends('layouts.main')
@section('content')

          

                <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-6'>
                        	@if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <div class='box'>
                                   <div class='box-body pad'>
                                   	<!--page data here-->
                               
         {{ Form::open(array('route'=>'transmissions.store')) }}
    
        <div class="form-group">              	
	        {{ Form::label('transmission_name', 'Transmission Type') }}
	        {{ Form::text('transmission_name', Input::old('transmission_name'), array('placeholder'=>'Transmission Type', 'class'=>'form-control')) }}  
        </div> 
        
        <div class="form-group">              	
	      {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}  

	         <a href="{{ URL::to('transmissions') }}" class="btn btn-default btn-cons pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>
        </div>


  

                                   	<!--end custom page data here-->
                                </div>
                            </div><!-- /.box -->

                          
                        </div><!-- /.col-->
                    </div><!-- ./row -->



                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop