@extends('layouts.main')
@section('content')

          

                <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-6'>
                        	@if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <div class='box'>
                                   <div class='box-body pad'>
                                   	<!--page data here-->
                               
         {{ Form::open(array('route'=>'conditions.store')) }}
    
        <div class="form-group">              	
	        {{ Form::label('condition_name', 'Vehicle Condition Name') }}
	        {{ Form::text('condition_name', Input::old('condition_name'), array('placeholder'=>'Vehicle Condition Name', 'class'=>'form-control')) }}  
        </div> 
        
        <div class="form-group">              	
	      {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}  

	         <a href="{{ URL::to('conditions') }}" class="btn btn-default btn-cons pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>
        </div>


  

                                   	<!--end custom page data here-->
                                </div>
                            </div><!-- /.box -->

                          
                        </div><!-- /.col-->
                    </div><!-- ./row -->



                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop