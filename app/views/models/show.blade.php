@extends('layouts.main')
@section('content')

          

                <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-6'>
                        	@if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <div class='box'>
                                   <div class='box-body pad'>
                                   	<!--page data here-->
                               
         {{ Form::model($model) }}
    
         <div class="form-group">               
            {{ Form::label('model_name', 'Make Name') }}
            {{ Form::text('make_name', $model->make['make_name'], array('placeholder'=>'Model Name', 'class'=>'form-control', 'readonly')) }}  
        </div> 
        <div class="form-group">              	
	        {{ Form::label('model_name', 'Model Name') }}
	        {{ Form::text('model_name', Input::old('model_name'), array('placeholder'=>'Model Name', 'class'=>'form-control', 'readonly')) }}  
        </div> 
        
        <div class="form-group">              	
		         <a href="{{ URL::to('models') }}" class="btn btn-default btn-cons pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
        </div>


  

                                   	<!--end custom page data here-->
                                </div>
                            </div><!-- /.box -->

                          
                        </div><!-- /.col-->
                    </div><!-- ./row -->



                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop