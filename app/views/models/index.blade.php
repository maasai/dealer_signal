@extends('layouts.main')
@section('content')

          

                <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='box'>
                                   <div class='box-body pad table-responsive'>
                                   	<!--page data here-->
                                  
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Model</th>
                                        <th>Make</th>
                          
                                    </tr>                                    
                                </thead>
                                <tbody>
<?php $count = $models->getFrom(); ?>
    @foreach ($models as $model)
        <tr>
            <td>{{ $count }}</td>
            <td>{{ $model->model_name }}</td>
             <td>{{ $model->make['make_name'] }}</td>
      
            <td class="no-print">
 <a href="{{ URL::to('models/'.$model->id.'/edit') }}" class="btn btn-info pull-right btn-sm"><i class="fa fa-edit"></i> Edit</a>
            </td>
            <td class="no-print">
                {{ Form::open(array('method'=> 'DELETE', 'route' => array('models.destroy', $model->id))) }}
                {{Form::button('<i class="fa fa-times"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick'=>'if(!confirm("Are you sure to delete this item? This cannot be undone.")){return false;}'))}}
                {{ Form::close() }}
            </td>

        </tr>
        <?php $count++; ?>
    @endforeach
                               </tbody>
                            </table>      
                                                 
                      
                                   	<!--end custom page data here-->
                                </div>
                            </div><!-- /.box -->

                          
                        </div><!-- /.col-->
                    </div><!-- ./row -->

   <div class="row no-print">
                         {{$models->links()}} 
                        <div class="col-xs-12">
                            <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                            <a href="{{ URL::to('models/create') }}" class="btn btn-success pull-right"><i class="fa fa-plus-square"></i> New Model</a>
                        
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop