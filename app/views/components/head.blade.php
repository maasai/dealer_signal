<meta charset="UTF-8">
        <title>Dealer Signal - Auto dealer backend, API, Android App</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {{ HTML::style('assets/css/bootstrap.min.css') }}

        <!-- font Awesome -->
        {{ HTML::style('assets/css/font-awesome.min.css') }}
        <!-- Ionicons -->
        {{ HTML::style('assets/css/ionicons.min.css') }}
        <!-- Morris chart -->
         {{ HTML::style('assets/css/morris/morris.css') }}
        <!-- jvectormap -->
         {{ HTML::style('assets/css/jvectormap/jquery-jvectormap-1.2.2.css') }}
        <!-- fullCalendar -->
         {{ HTML::style('assets/css/fullcalendar/fullcalendar.css') }}
        <!-- Daterange picker -->
         {{ HTML::style('assets/css/daterangepicker/daterangepicker-bs3.css') }}
        <!-- bootstrap wysihtml5 - text editor -->
        {{ HTML::style('assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
        <!-- Theme style -->
        {{ HTML::style('assets/css/AdminLTE.css') }}
        {{ HTML::style('assets/css/custom.css') }}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->