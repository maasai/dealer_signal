            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                              <!-- Sidebar user panel -->
                    <div class="user-panel">

                        <div class="pull-left info">
                            @if(Auth::check())
                            <p>Hello, {{ ucfirst(Auth::user()->first_name)}}</p>
                            @endif

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        <br/><br/>
                            <a href="{{ URL::to('logout') }}"><i class="fa fa-power-off text-success"></i> Log Out</a>

                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li
                         @if(isset($menu) && $menu == 'dashboard')
                              class="active"      
                        @endif
                         ><a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li
                        @if(isset($menu) && $menu == 'listings')
                              class="active"
                        @endif
                        >
                            <a href="{{ URL::to('listings')}}">
                                <i class="fa fa-list-ul"></i> <span>Vehicle Listings</span> <small class="badge pull-right bg-green">590</small>
                            </a>
                        </li>
                         <li
                        @if(isset($menu) && $menu == 'featured')
                        class="active"      
                        @endif
                         >
                            <a href="#">
                                <i class="fa fa-list-ol"></i> <span>Featured Vehicles</span> <small class="badge pull-right bg-green">150</small>
                            </a>
                        </li>
                        <li
                    @if(isset($menu) && $menu == 'features')
                        class="active"      
                        @endif
                        >
                            <a href="{{ URL::to('features')}}">
                                <i class="fa fa-th-large"></i> <span>Vehicle Features</span> 
                            </a>
                        </li>

                        <li
                    @if(isset($menu) && $menu == 'categories')
                        class="active"      
                        @endif
                        >
                            <a href="{{ URL::to('categories')}}">
                                <i class="fa fa-th-large"></i> <span>Vehicle Categories</span> 
                            </a>
                        </li>

                                             
                        <li
                @if(isset($menu) && $menu == 'makes')
                        class="treeview active"      
                        @endif
                         class="treeview"
                        >
                            <a href="#">
                                <i class="fa fa-th-large"></i>
                                <span>Makes</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('makes')}}"><i class="fa fa-angle-double-right"></i> All Makes</a></li>
                                <li><a href="{{ URL::to('makes/create')}}"><i class="fa fa-angle-double-right"></i> New Make</a></li>
                             </ul>
                        </li>
                          <li
                    @if(isset($menu) && $menu == 'models')
                        class="treeview active"      
                        @endif
                         class="treeview"
                          >
                            <a href="#">
                                <i class="fa fa-th-large"></i>
                                <span>Models</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('models')}}"><i class="fa fa-angle-double-right"></i> All Models</a></li>
                                <li><a href="{{ URL::to('models/create')}}"><i class="fa fa-angle-double-right"></i> New Model</a></li>
                             </ul>
                        </li>
                        <li
                @if(isset($menu) && $menu == 'users')
                        class="treeview active"      
                        @endif
                         class="treeview"
                        >
                            <a href="#">
                                <i class="fa fa-user"></i> <span>System Users</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
        <li>{{ HTML::link('users', ' All Users', array('class'=>'fa fa-angle-double-right'))}}</li>
        <li>{{ HTML::link('users/create', ' New User', array('class'=>'fa fa-angle-double-right'))}}</li>  
                            </ul>
                        </li>

                          <li
                    @if(isset($menu) && $menu == 'settings')
                        class="treeview active"      
                        @endif
                         class="treeview"
                          >
                            <a href="#">
                                <i class="fa fa-gear"></i> <span>More Settings</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                           <ul class="treeview-menu">
                                <li><a href="{{ URL::to('transmissions')}}"></i> Transmissions</a></li>
                                <li><a href="{{ URL::to('conditions')}}"></i> Condition</a></li>
                                <li><a href="{{ URL::to('fuels')}}"></i> Fuel Type</a></li>
                                <li><a href="{{ URL::to('colors')}}"></i> Colors</a></li>
                                 <li><a href="{{ URL::to('pages')}}"></i> System Pages</a></li>
                                                  
                        </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>