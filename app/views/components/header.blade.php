<a href="#" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Dealer Signal
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                         <li class="dropdown user user-menu">
                             <a href="{{ URL::to('logout') }}"><i class="fa fa-power-off text-success"></i> Log Out</a>

                        </li>
                    </ul>
                </div>
            </nav>