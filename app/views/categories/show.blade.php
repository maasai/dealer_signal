@extends('layouts.main')
@section('content')

          

                <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-6'>
                        	@if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <div class='box'>
                                   <div class='box-body pad'>
                                   	<!--page data here-->
                               
           {{ Form::model($category) }}
    
        <div class="form-group">              	
	        {{ Form::label('category_name', 'Category Name') }}
	        {{ Form::text('category_name', Input::old('category_name'), array('placeholder'=>'Category Name', 'class'=>'form-control', 'readonly')) }}  
        </div> 
        
        <div class="form-group">              	
	         <a href="{{ URL::to('categories') }}" class="btn btn-default btn-cons pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
        </div>


  

                                   	<!--end custom page data here-->
                                </div>
                            </div><!-- /.box -->

                          
                        </div><!-- /.col-->
                    </div><!-- ./row -->



                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop