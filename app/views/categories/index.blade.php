@extends('layouts.main')
@section('content')

          

                <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='box'>
                                   <div class='box-body pad table-responsive'>
                                   	<!--page data here-->
                                  
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Vehicle Category</th>
                          
                                    </tr>                                    
                                </thead>
                                <tbody>
<?php $count = $categories->getFrom(); ?>
    @foreach ($categories as $category)
        <tr>
            <td>{{ $count }}</td>
            <td>{{ $category->category_name }}</td>
      
            <td class="no-print">
 <a href="{{ URL::to('categories/'.$category->id.'/edit') }}" class="btn btn-info pull-right btn-sm"><i class="fa fa-edit"></i> Edit</a>
            </td>
            <td class="no-print">
                {{ Form::open(array('method'=> 'DELETE', 'route' => array('categories.destroy', $category->id))) }}
                {{Form::button('<i class="fa fa-times"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick'=>'if(!confirm("Are you sure to delete this item? This cannot be undone.")){return false;}'))}}
                {{ Form::close() }}
            </td>

        </tr>
        <?php $count++; ?>
    @endforeach
                               </tbody>
                            </table>      
                                                 
                      
                                   	<!--end custom page data here-->
                                </div>
                            </div><!-- /.box -->

                          
                        </div><!-- /.col-->
                    </div><!-- ./row -->

   <div class="row no-print">
                         {{$categories->links()}} 
                        <div class="col-xs-12">
                            <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                            <a href="{{ URL::to('categories/create') }}" class="btn btn-success pull-right"><i class="fa fa-plus-square"></i> New Category</a>
                        
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop