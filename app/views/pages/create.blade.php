@extends('layouts.main')
@section('content')

          

                <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-12'>
                        	@if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <div class='box'>
                                   <div class='box-body pad'>
                                   	<!--page data here-->
                               
         {{ Form::open(array('route'=>'pages.store')) }}
    
        <div class="form-group">              	
	        {{ Form::label('page_name', 'Page Name') }}
	        {{ Form::text('page_name', Input::old('page_name'), array('placeholder'=>'Page Name', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group">              	
	        {{ Form::label('page_content', 'Page Content') }}
	        {{ Form::textarea('page_content', Input::old('page_content'), array('placeholder'=>'Page Content', 'class'=>'form-control textarea')) }}  
        </div> 
        
        <div class="form-group">              	
	      {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}  

	         <a href="{{ URL::to('pages') }}" class="btn btn-default btn-cons pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>
        </div>

                           	<!--end custom page data here-->
                                </div>
                            </div><!-- /.box -->

                          
                        </div><!-- /.col-->
                    </div><!-- ./row -->



                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop