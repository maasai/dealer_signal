<?php

use Dealer\Repositories\Model\ModelRepository;
use Dealer\Transformers\ModelTransformer;

class ModelsController extends ApiController {
    /**
     * @var modelTransformer
     */
    private $modelTransformer;

    /**
     * @var modelRepository
     */
    private $modelRepository;

    /**
     * We need both repository and transformer for out data
     * @param ModelRepository $modelRepository
     * @param ModelTransformer $modelTransformer
     */
    function __construct(ModelRepository $modelRepository, ModelTransformer $modelTransformer)
    {
        $this->modelTransformer = $modelTransformer;
        $this->modelRepository = $modelRepository;
    }

	/**
	 * Display a listing of the model.
	 * GET /models
	 *
	 * @return Response
	 */
	public function index()
	{
        $models = $this->modelRepository->getAll('make');

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$models)
            {
                return $this->respondNotFound("vehicle model not found !!");
            }
            return $this->respondWithPagination($models,[
                'data' => $this->modelTransformer->transformCollection($models->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'models' => $models,
			'title' => 'All Models',
			'menu'	=> 'models'
			);

		return View::make('models.index', $this->data);
	}

	/**
	 * Show the form for creating a new model.
	 * GET /models/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$makes = Make::all()->lists('make_name', 'id');
		$this->data = array(
			'title' => 'New Model',
			'menu' 	=>	'models' ,
			'makes'	=>	$makes
			);
		return View::make('models.create', $this->data);
	}

	/**
	 * Store a newly created model in storage.
	 * POST /models
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Model::$rules);
		if($validator->passes())
		{
			$model = new Model();
			$model->model_name = Input::get('model_name');
			$model->make_id = Input::get('make_id');
			
			if($model->save())
			{

			return Redirect::to('models')
				->with('message', 'Success !! New model created.');
			}

			
		}else
		return Redirect::to('models/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified model.
	 * GET /models/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $model = $this->modelRepository->getByID($id);

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$model)
            {
                return $this->respondNotFound("Vehicle model not found !!");
            }
            return $this->respond([
                'data' => $this->modelTransformer->transform($model)
            ] );
        }

        //If normal route access, give them the view

		if(!is_null($model)){
			$this->data = array(
				'model'  => $model,
				'title' => 'model',
				'menu'	=>	'models'
				);
		return View::make('models.show', $this->data);
		}
		return Redirect::to('models')
			->with('message', 'Model could not be found.');

	}

	/**
	 * Show the form for editing the specified model.
	 * GET /models/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$makes = Make::all()->lists('make_name', 'id');
		$model = model::find($id);
		if(is_null($model))
		{
			return Redirect::to('models')->with('message', 'Model not found. Please try another.' );
		}
		$this->data = array(
			'title' => 'Edit model',
			'model' => $model,
			'menu'	=>	'models',
			'makes' => $makes,

			);

		return View::make('models.edit', $this->data);
	}

	/**
	 * Update the specified model in storage.
	 * PUT /models/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$model = model::find($id);
		if(!is_null($model)){
			$rules = array(
				'model_name' => 'required|min:1|unique:models,model_name,'.$id.',id,deleted_at,NULL'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$model->model_name = Input::get('model_name');
					$model->make_id = Input::get('make_id');

					
					if($model->update())
					{
					return Redirect::to('models')
							->with('message', 'Success !! Vehicle model updated.');
					}
			}
			return Redirect::route('models.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('models')
			->with('message', 'Model not available. Please Retry.');
	}

	/**
	 * Remove the specified model from storage.
	 * DELETE /models/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$model = model::find($id);
		if(!is_null($model)){
			
			//delete the model record
			$model->delete();
			return Redirect::to('models')
				->with('message', 'Success !! Model has been deleted.');
		}
		return Redirect::to('models')
			->with('message', 'Model not available. Please Retry.');
	}


}