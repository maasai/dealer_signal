<?php

use Dealer\Repositories\User\UserRepository;
use Dealer\Transformers\UserTransformer;

class UsersController extends ApiController {

    /**
     * @var userTransformer
     */
    private $userTransformer;

    /**
     * @var userRepository
     */
    private $userRepository;

    /**
     * We need both repository and transformer for out data
     * @param userRepository $userRepository
     * @param userTransformer $userTransformer
     */
    function __construct(UserRepository $userRepository, UserTransformer $userTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->userRepository = $userRepository;
    }

   	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $users = $this->userRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$users)
            {
                return $this->respondNotFound("User not found !!");
            }
            return $this->respondWithPagination($users,[
                'data' => $this->userTransformer->transformCollection($users->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'users' => $users,
			'title' => 'Registered System Users',
			'menu'	=>	'users'
			);
		return View::make('users.index', $this->data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'Create New System User',
			'menu'	=>	'users'

			);
		return View::make('users.create', $this->data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input  = Input::all();
		$validator = Validator::make($input, User::$rules_create);
		if($validator->passes()){
			$user = new User();
			$user->first_name = Input::get('first_name');
			$user->last_name = Input::get('last_name');
			$user->email = Input::get('email');
			$user->activated = 1;
			$user->password = Hash::make(Input::get('password'));
			$user->save();

			return Redirect::to('users')->with('message', 'Success !! User has been created.');
		}
		return Redirect::back()->withErrors($validator)
		->with('message', 'Oops. There were errors in the form.')
		->withInput();

		//return "Save the created user and redirect";
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return $this->index();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		if(is_null($user))
		{
			return Redirect::to('users')->with('message', 'The user doesn\'t exist' );
		}
		$this->data = array(
			'title' => 'Edit Sytem user',
			'user' => $user,
			'menu'	=>	'users'
			);

		return View::make('users.edit', $this->data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input  = Input::all();
		User::$rules_create['email'] = 'required|email|unique:users,email,'.$id;
		$validator = Validator::make($input, User::$rules_create);
		if($validator->passes()){
			$user = User::find($id);
			$user->first_name = Input::get('first_name');
			$user->last_name = Input::get('last_name');
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->update($input);
			return Redirect::route('users.index')->with('message', 'Success !! User has been updated. ');
		}
		return Redirect::route('users.edit', $id)
		->withErrors($validator)
		->withInput()
		->with('message', 'There were validation errors.');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);
		return Redirect::back()->with('message', 'User has been deleted successfully.');
	}

    /**
     * Sign the user in the system
     */

    public function getLogin(){
        return View::make('users.account.login');

    }
    /**
     * Process the signin form
     * @return [type]
     */
    public function postLogin(){
        $rules_login = array(

        );
        $validator = Validator::make(Input::all(), $rules_login);
        if ($validator->passes()) {
            $details = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );

            if(Auth::attempt($details)){
                return Redirect::to('users');
            }
            return Redirect::to('login')
                ->with('message',  'Invalid email / password. Please retry.');
        }
        return Redirect::to('login')
            ->withErrors($validator)
            ->withInput();

    }

    /**
     * Signout the user
     * @return [type]
     */
    public function getLogout(){
        if (Auth::check()) {
            Auth::logout();
            return Redirect::to('login');
        }
        return Redirect::to('login');
    }


}
