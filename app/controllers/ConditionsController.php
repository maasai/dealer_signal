<?php

use Dealer\Repositories\Condition\ConditionRepository;
use Dealer\Transformers\ConditionTransformer;

class ConditionsController extends ApiController {
    /**
     * @var conditionTransformer
     */
    private $conditionTransformer;

    /**
     * @var conditionRepository
     */
    private $conditionRepository;

    /**
     * We need both repository and transformer for out data
     * @param conditionRepository $conditionRepository
     * @param conditionTransformer $conditionTransformer
     */
    function __construct(ConditionRepository $conditionRepository, ConditionTransformer $conditionTransformer)
    {
        $this->conditionTransformer = $conditionTransformer;
        $this->conditionRepository = $conditionRepository;
    }

    /**
	 * Display a listing of the condition.
	 * GET /conditions
	 *
	 * @return Response
	 */
	public function index()
	{
        $conditions = $this->conditionRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$conditions)
            {
                return $this->respondNotFound("Condition not found !!");
            }
            return $this->respondWithPagination($conditions,[
                'data' => $this->conditionTransformer->transformCollection($conditions->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'conditions' => $conditions,
			'title' => 'Vehicle Condition States',
			'menu'	=> 'settings'
			);

		return View::make('conditions.index', $this->data);
	}

	/**
	 * Show the form for creating a new condition.
	 * GET /conditions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'New Condition State',
			'menu' 	=>	'settings' 
			);
		return View::make('conditions.create', $this->data);
	}

	/**
	 * Store a newly created condition in storage.
	 * POST /conditions
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Condition::$rules);
		if($validator->passes())
		{
			$condition = new Condition();
			$condition->condition_name = Input::get('condition_name');
			
			if($condition->save())
			{

			return Redirect::to('conditions')
				->with('message', 'Success !! New Condition status created.');
			}

			
		}else
		return Redirect::to('conditions/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified condition.
	 * GET /conditions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $condition = $this->conditionRepository->getByID($id);

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$condition)
            {
                return $this->respondNotFound("Condition not found !!");
            }
            return $this->respond([
                'data' => $this->conditionTransformer->transform($condition)
            ] );
        }

        //If normal route access, give them the view
		if(!is_null($condition)){
			$this->data = array(
				'condition'  => $condition,
				'title' => 'Condition Status',
				'menu'	=>	'settings'
				);
		return View::make('conditions.show', $this->data);
		}
		return Redirect::to('conditions')
			->with('message', 'Condition status could not be found.');

	}

	/**
	 * Show the form for editing the specified condition.
	 * GET /conditions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$condition = Condition::find($id);
		if(is_null($condition))
		{
			return Redirect::to('conditions')->with('message', 'Vehicle Condition not found. Please try another.' );
		}
		$this->data = array(
			'title' => 'Edit Condition',
			'condition' => $condition,
			'menu'	=>	'settings'
			);

		return View::make('conditions.edit', $this->data);
	}

	/**
	 * Update the specified condition in storage.
	 * PUT /conditions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$condition = Condition::find($id);
		if(!is_null($condition)){
			$rules = array(
				'condition_name' => 'required|min:1|unique:conditions,condition_name,'.$id.',id,deleted_at,NULL'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$condition->condition_name = Input::get('condition_name');
					
					if($condition->update())
					{
					return Redirect::to('conditions')
							->with('message', 'Success !! Vehicle condition updated.');
					}
			}
			return Redirect::route('conditions.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('conditions')
			->with('message', 'Vehicle Condition not available. Please Retry.');
	}

	/**
	 * Remove the specified condition from storage.
	 * DELETE /conditions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$condition = Condition::find($id);
		if(!is_null($condition)){
			
			//delete the condition record
			$condition->delete();
			return Redirect::to('conditions')
				->with('message', 'Success !! Vehicle condition has been deleted.');
		}
		return Redirect::to('conditions')
			->with('message', 'vehicle condition not available. Please Retry.');
	}


}