<?php

use Dealer\Repositories\Page\PageRepository;
use Dealer\Transformers\PageTransformer;

class PagesController extends ApiController {

    /**
     * @var pageTransformer
     */
    private $pageTransformer;

    /**
     * @var pageRepository
     */
    private $pageRepository;

    /**
     * We need both repository and transformer for out data
     * @param pageRepository $pageRepository
     * @param pageTransformer $pageTransformer
     */
    function __construct(PageRepository $pageRepository, PageTransformer $pageTransformer)
    {
        $this->pageTransformer = $pageTransformer;
        $this->pageRepository = $pageRepository;
    }

	/**
	 * Display a listing of the page.
	 * GET /pages
	 *
	 * @return Response
	 */
	public function index()
	{
        $pages = $this->pageRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$pages)
            {
                return $this->respondNotFound("Page not found !!");
            }
            return $this->respondWithPagination($pages,[
                'data' => $this->pageTransformer->transformCollection($pages->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'pages' => $pages,
			'title' => 'All Pages',
			'menu'	=> 'settings'
			);

		return View::make('pages.index', $this->data);
	}

	/**
	 * Show the form for creating a new page.
	 * GET /pages/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'New Page',
			'menu' 	=>	'settings' 
			);
		return View::make('pages.create', $this->data);
	}

	/**
	 * Store a newly created page in storage.
	 * POST /pages
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Page::$rules);
		if($validator->passes())
		{
			$page = new Page();
			$page->page_name 	= Input::get('page_name');
			$page->page_content = Input::get('page_content');
			
			if($page->save())
			{

			return Redirect::to('pages')
				->with('message', 'Success !! New page created.');
			}

			
		}else
		return Redirect::to('pages/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified page.
	 * GET /pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $page = $this->pageRepository->getByID($id);

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$page)
            {
                return $this->respondNotFound("Page not found !!");
            }
            return $this->respond([
                'data' => $this->pageTransformer->transform($page)
            ] );
        }

        //If normal route access, give them the view
		if(!is_null($page)){
			$this->data = array(
				'page'  => $page,
				'title' => 'Page',
				'menu'	=>	'settings'
				);
		return View::make('pages.show', $this->data);
		}
		return Redirect::to('pages')
			->with('message', 'Page could not be found.');

	}

	/**
	 * Show the form for editing the specified page.
	 * GET /pages/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = Page::find($id);
		if(is_null($page))
		{
			return Redirect::to('pages')->with('message', 'Page not found. Please try another.' );
		}
		$this->data = array(
			'title' => 'Edit Page',
			'page' => $page,
			'menu'	=>	'settings'
			);

		return View::make('pages.edit', $this->data);
	}

	/**
	 * Update the specified page in storage.
	 * PUT /pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$page = Page::find($id);
		if(!is_null($page)){
			$rules = array(
				'page_name' 	=> 'required|min:1|unique:pages,page_name,'.$id.',id,deleted_at,NULL',
				'page_content'	=> 'required'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$page->page_name = Input::get('page_name');
					$page->page_content	= Input::get('page_content');
					
					if($page->update())
					{
					return Redirect::to('pages')
							->with('message', 'Success !! Page updated.');
					}
			}
			return Redirect::route('pages.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('pages')
			->with('message', 'Page not available. Please Retry.');
	}

	/**
	 * Remove the specified page from storage.
	 * DELETE /pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$page = Page::find($id);
		if(!is_null($page)){
			
			//delete the page record
			$page->delete();
			return Redirect::to('pages')
				->with('message', 'Success !! Page has been deleted.');
		}
		return Redirect::to('pages')
			->with('message', 'Page not available. Please Retry.');
	}


}