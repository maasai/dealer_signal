<?php

use Dealer\Repositories\Category\CategoryRepository;
use Dealer\Transformers\CategoryTransformer;

class CategoriesController extends ApiController {

    /**
     * @var categoryTransformer
     */
    private $categoryTransformer;

    /**
     * @var categoryRepository
     */
    private $categoryRepository;

    /**
     * We need both repository and transformer for out data
     * @param categoryRepository $categoryRepository
     * @param categoryTransformer $categoryTransformer
     */
    function __construct(CategoryRepository $categoryRepository, CategoryTransformer $categoryTransformer)
    {
        $this->categoryTransformer = $categoryTransformer;
        $this->categoryRepository = $categoryRepository;
    }

	/**
	 * Display a listing of the category.
	 * GET /categories
	 *
	 * @return Response
	 */
	public function index()
	{
        $categories = $this->categoryRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$categories)
            {
                return $this->respondNotFound("Category not found !!");
            }
            return $this->respondWithPagination($categories,[
                'data' => $this->categoryTransformer->transformCollection($categories->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'categories' => $categories,
			'title' => 'All Categories',
			'menu'	=> 'categories'
			);

		return View::make('categories.index', $this->data);
	}

	/**
	 * Show the form for creating a new category.
	 * GET /categories/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'New Category',
			'menu' 	=>	'categories' 
			);
		return View::make('categories.create', $this->data);
	}

	/**
	 * Store a newly created category in storage.
	 * POST /categories
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Category::$rules);
		if($validator->passes())
		{
			$category = new Category();
			$category->category_name = Input::get('category_name');
			
			if($category->save())
			{

			return Redirect::to('categories')
				->with('message', 'Success !! New category created.');
			}

			
		}else
		return Redirect::to('categories/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified category.
	 * GET /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $category = $this->categoryRepository->getByID($id);

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$category)
            {
                return $this->respondNotFound("Category not found !!");
            }
            return $this->respond([
                'data' => $this->categoryTransformer->transform($category)
            ] );
        }

        //If normal route access, give them the view
		if(!is_null($category)){
			$this->data = array(
				'category'  => $category,
				'title' => 'Category',
				'menu'	=>	'categories'
				);
		return View::make('categories.show', $this->data);
		}
		return Redirect::to('categories')
			->with('message', 'Category could not be found.');

	}

	/**
	 * Show the form for editing the specified category.
	 * GET /categories/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Category::find($id);
		if(is_null($category))
		{
			return Redirect::to('categories')->with('message', 'Category not found. Please try another.' );
		}
		$this->data = array(
			'title' => 'Edit Category',
			'category' => $category,
			'menu'	=>	'categories'
			);

		return View::make('categories.edit', $this->data);
	}

	/**
	 * Update the specified category in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$category = Category::find($id);
		if(!is_null($category)){
			$rules = array(
				'category_name' => 'required|min:1|unique:categories,category_name,'.$id.',id,deleted_at,NULL'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$category->category_name = Input::get('category_name');
					
					if($category->update())
					{
					return Redirect::to('categories')
							->with('message', 'Success !! Vehicle Category updated.');
					}
			}
			return Redirect::route('categories.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('categories')
			->with('message', 'Category not available. Please Retry.');
	}

	/**
	 * Remove the specified category from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$category = Category::find($id);
		if(!is_null($category)){
			
			//delete the category record
			$category->delete();
			return Redirect::to('categories')
				->with('message', 'Success !! Category has been deleted.');
		}
		return Redirect::to('categories')
			->with('message', 'Category not available. Please Retry.');
	}


}