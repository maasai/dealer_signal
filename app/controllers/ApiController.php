<?php

use Illuminate\Pagination\Paginator;


class ApiController extends BaseController{

    protected $statusCode = 200;

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * The response status code
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Gives the resource collection with pagination
     * @param Paginator $items
     * @param $data
     * @return mixed
     */

    protected function respondWithPagination(Paginator $items, $data)
    {
        $data = array_merge($data,[
            'paginator' => [
                'total_count' 	=> $items->getTotal(),
                'total_pages' 	=> ceil($items->getTotal() / $items->getPerPage()),
                'current_page'	=>	$items->getCurrentPage(),
                'limit'			=>	$items->getPerPage()
            ]
        ]);

        return $this->respond($data);

    }

    /**
     * When a missing resource is requested
     * @param string $message
     * @return mixed
     */
    public function respondNotFound($message = "Not Found !")
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * When a non supported search parameter is requested
     * @param string $message
     * @return mixed
     */
    public function respondWrongParameter ($message = "You requested a non supported search parameter!")
    {
        return $this->setStatusCode(400)->respondWithError($message);
    }

    /**
     * There was an internal error
     * @param string $message
     * @return mixed
     */
    public function respondInternalError($message = "Internal Server Error !!")
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }


    /**
     * Give json feedback with status code
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);

    }

    /**
     * respond with a generic error
     * @param string $message
     * @return mixed
     */
    public function respondWithError($message  = 'There was an error')
    {
        return $this->respond([
            'error' => [
                'error'         => true,
                'message'       => $message,
                'status_code'   => $this->getStatusCode()
            ]
        ]);

    }

    public static function ignoreWords($list, $ignoreList)
    {

        foreach ($ignoreList as &$word) {
            $word = '/\b' . preg_quote($word, '/') . '\b/';
        }

        return preg_replace($ignoreList, '', $list);

    }

}