<?php

class ListingsController extends ApiController
{

    /**
     * @var Dealer\Transformers\ListingTransformer
     */
    private $listingTransformer;

    /**
     * @var Dealer\Repositories\Listing\ListingRepository
     */
    private $listingRepository;

    /**
     * @var Dealer\Repositories\Make\MakeRepository
     */
    private $makeRepository;

    /**
     * @var Dealer\Repositories\Feature\FeatureRepository
     */
    private $featureRepository;

    /**
     * @var Dealer\Repositories\Category\CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var Dealer\Repositories\Model\ModelRepository
     */
    private $modelRepository;

    /**
     * @var Dealer\Repositories\Condition\ConditionRepository
     */
    private $conditionRepository;

    /**
     * @var Dealer\Repositories\Fuel\FuelRepository
     */
    private $fuelRepository;

    /**
     * @var Dealer\Repositories\Transmission\TransmissionRepository
     */
    private $transmissionRepository;

    /**
     * @var Dealer\Repositories\Color\ColorRepository
     */
    private $colorRepository;

    /**
     * @var Dealer\Service\Form\Listing\ListingValidator
     */
    protected $validator;

    /**
     * @var Dealer\Service\Form\Listing\ListingForm
     */
    protected $listingForm;

    /**
     * Inject dependencies via IoC Container
     */
    function __construct()
    {
        $this->validator = App::make('Dealer\Service\Form\Listing\ListingValidator');
        $this->listingForm = App::make('Dealer\Service\Form\Listing\ListingForm');
        $this->listingTransformer = App::make('Dealer\Transformers\ListingTransformer');
        $this->listingRepository = App::make('Dealer\Repositories\Listing\ListingRepository');
        $this->makeRepository = App::make('Dealer\Repositories\Make\MakeRepository');
        $this->modelRepository = App::make('Dealer\Repositories\Model\ModelRepository');
        $this->categoryRepository = App::make('Dealer\Repositories\Category\CategoryRepository');
        $this->featureRepository = App::make('Dealer\Repositories\Feature\FeatureRepository');
        $this->conditionRepository = App::make('Dealer\Repositories\Condition\ConditionRepository');
        $this->fuelRepository = App::make('Dealer\Repositories\Fuel\FuelRepository');
        $this->colorRepository = App::make('Dealer\Repositories\Color\ColorRepository');
        $this->transmissionRepository = App::make('Dealer\Repositories\Transmission\TransmissionRepository');
    }

    /**
     * Display a listing of the resource.
     * GET /listings
     *
     * @return Response
     */
    public function index()
    {
        $load = array('make', 'model', 'features');
        $listings = $this->listingRepository->getAll($load);
        //Respond with JSON if api route is requested
        if (Request::is('api/*')) {
            if (!$listings) {
                return $this->respondNotFound("Listing not found !!");
            }
            return $this->respondWithPagination($listings, [
                'data' => $this->listingTransformer->transformCollection($listings->all())
            ]);
        }

        //If normal route access, give them the view
        $this->data = array(
            'listings' => $listings,
            'title' => 'All listings',
            'menu' => 'listings'
        );

        return View::make('listings.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * GET /listings/create
     *
     * @return Response
     */
    public function create()
    {
        $load = array();
        $makes = $this->makeRepository->getAll()->lists('make_name', 'id');
        $categories = $this->categoryRepository->getAll()->lists('category_name', 'id');
        $models = $this->modelRepository->getAll()->lists('model_name', 'id');
        $features = $this->featureRepository->getAll($load, false)->lists('feature_name', 'id');
        $conditions = $this->conditionRepository->getAll()->lists('condition_name', 'id');
        $fuels = $this->fuelRepository->getAll()->lists('fuel_name', 'id');
        $transmissions = $this->transmissionRepository->getAll()->lists('transmission_name', 'id');
        $colors = $this->colorRepository->getAll()->lists('color_name', 'id');

        $this->data = array(
            'title' => 'Add Vehicle Listing',
            'makes' => $makes,
            'models' => $models,
            'categories' => $categories,
            'features' => $features,
            'conditions' => $conditions,
            'fuels' => $fuels,
            'transmissions' => $transmissions,
            'colors' => $colors,
            'menu' => 'listings'
        );
        return View::make('listings.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * POST /listings
     *
     * @return Response
     */
    public function store()
    {
     if($this->listingForm->save(Input::all())){
            return Redirect::to('listings')
                ->withInput()
                ->with('message', 'Success !! Vehicle listing has been created.');
     }else
         return Redirect::to('listings/create')
                ->withInput()
                ->withErrors( $this->listingForm->errors() )
                ->with('message', 'Oops. There were validation errors.');
    }

    /**
     * Display the specified resource.
     * GET /listings/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /listings/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $filters = array();
        $load = ['features'];
        $listing = $this->listingRepository->getByID($id, $filters, $load);
      //  $selectedFeatures = $listing->features();

        $load = array();
        $makes = $this->makeRepository->getAll()->lists('make_name', 'id');
        $categories = $this->categoryRepository->getAll()->lists('category_name', 'id');
        $models = $this->modelRepository->getAll()->lists('model_name', 'id');
        $features = $this->featureRepository->getAll($load, false)->lists('feature_name', 'id');
        $conditions = $this->conditionRepository->getAll()->lists('condition_name', 'id');
        $fuels = $this->fuelRepository->getAll()->lists('fuel_name', 'id');
        $transmissions = $this->transmissionRepository->getAll()->lists('transmission_name', 'id');
        $colors = $this->colorRepository->getAll()->lists('color_name', 'id');

        $this->data = array(
            'title' => 'Edit Listing',
            'makes' => $makes,
            'models' => $models,
            'categories' => $categories,
            'features' => $features,
            'conditions' => $conditions,
            'fuels' => $fuels,
            'transmissions' => $transmissions,
            'colors' => $colors,
            'listing' => $listing,
            'menu' => 'listings'
        );
        return View::make('listings.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * PUT /listings/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /listings/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if($this->listingForm->delete($id)){
            return Redirect::back()
                ->with('message', 'Success !! Listing has been deleted.');
        }else{
            return Redirect::back()
                ->with('message', 'Listing is not found, please try again');
        }
    }

}