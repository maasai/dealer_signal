<?php

use Dealer\Repositories\Transmission\TransmissionRepository;
use Dealer\Transformers\TransmissionTransformer;

class TransmissionsController extends ApiController {

    /**
     * @var transmissionTransformer
     */
    private $transmissionTransformer;

    /**
     * @var transmissionRepository
     */
    private $transmissionRepository;

    /**
     * We need both repository and transformer for out data
     * @param transmissionRepository $transmissionRepository
     * @param transmissionTransformer $transmissionTransformer
     */
    function __construct(TransmissionRepository $transmissionRepository, TransmissionTransformer $transmissionTransformer)
    {
        $this->transmissionTransformer = $transmissionTransformer;
        $this->transmissionRepository = $transmissionRepository;
    }


    /**
	 * Display a listing of the transmission.
	 * GET /transmissions
	 *
	 * @return Response
	 */
	public function index()
	{
        $transmissions = $this->transmissionRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$transmissions)
            {
                return $this->respondNotFound("Transmission not found !!");
            }
            return $this->respondWithPagination($transmissions,[
                'data' => $this->transmissionTransformer->transformCollection($transmissions->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'transmissions' => $transmissions,
			'title' => 'Vehicle Transmission Types',
			'menu'	=> 'settings'
			);

		return View::make('transmissions.index', $this->data);
	}

	/**
	 * Show the form for creating a new transmission.
	 * GET /transmissions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'New Transmission Type',
			'menu' 	=>	'settings' 
			);
		return View::make('transmissions.create', $this->data);
	}

	/**
	 * Store a newly created transmission in storage.
	 * POST /transmissions
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Transmission::$rules);
		if($validator->passes())
		{
			$transmission = new Transmission();
			$transmission->transmission_name = Input::get('transmission_name');
			
			if($transmission->save())
			{

			return Redirect::to('transmissions')
				->with('message', 'Success !! New Transmission type created.');
			}

			
		}else
		return Redirect::to('transmissions/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified transmission.
	 * GET /transmissions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $transmission = $this->transmissionRepository->getByID($id);

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$transmission)
            {
                return $this->respondNotFound("Transmission not found !!");
            }
            return $this->respond([
                'data' => $this->transmissionTransformer->transform($transmission)
            ] );
        }

        //If normal route access, give them the view
		if(!is_null($transmission)){
			$this->data = array(
				'transmission'  => $transmission,
				'title' => 'Transmission Type',
				'menu'	=>	'settings'
				);
		return View::make('transmissions.show', $this->data);
		}
		return Redirect::to('transmissions')
			->with('message', 'Transmission type could not be found.');

	}

	/**
	 * Show the form for editing the specified transmission.
	 * GET /transmissions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$transmission = transmission::find($id);
		if(is_null($transmission))
		{
			return Redirect::to('transmissions')->with('message', 'Transmission type not found. Please try another.' );
		}
		$this->data = array(
			'title' => 'Edit Transmission Type',
			'transmission' => $transmission,
			'menu'	=>	'settings'
			);

		return View::make('transmissions.edit', $this->data);
	}

	/**
	 * Update the specified transmission in storage.
	 * PUT /transmissions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$transmission = Transmission::find($id);
		if(!is_null($transmission)){
			$rules = array(
				'transmission_name' => 'required|min:1|unique:transmissions,transmission_name,'.$id.',id,deleted_at,NULL'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$transmission->transmission_name = Input::get('transmission_name');
					
					if($transmission->update())
					{
					return Redirect::to('transmissions')
							->with('message', 'Success !! Vehicle Transmission type updated.');
					}
			}
			return Redirect::route('transmissions.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('transmissions')
			->with('message', 'Transmission type not available. Please Retry.');
	}

	/**
	 * Remove the specified transmission from storage.
	 * DELETE /transmissions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$transmission = Transmission::find($id);
		if(!is_null($transmission)){
			
			//delete the transmission record
			$transmission->delete();
			return Redirect::to('transmissions')
				->with('message', 'Success !! Transmission type has been deleted.');
		}
		return Redirect::to('transmissions')
			->with('message', 'Transmission type not available. Please Retry.');
	}


}