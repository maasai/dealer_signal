<?php

use Dealer\Repositories\Color\ColorRepository;
use Dealer\Transformers\ColorTransformer;

class ColorsController extends ApiController {

    /**
     * @var colorTransformer
     */
    private $colorTransformer;

    /**
     * @var colorRepository
     */
    private $colorRepository;

    /**
     * We need both repository and transformer for out data
     * @param ColorRepository $colorRepository
     * @param ColorTransformer $colorTransformer
     */
    function __construct(ColorRepository $colorRepository, ColorTransformer $colorTransformer)
    {
        $this->colorTransformer = $colorTransformer;
        $this->colorRepository = $colorRepository;
    }

	/**
	 * Display a listing of the color.
	 * GET /colors
	 *
	 * @return Response
	 */
	public function index()
	{
        $colors = $this->colorRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$colors)
            {
                return $this->respondNotFound("Color not found !!");
            }
            return $this->respondWithPagination($colors,[
                'data' => $this->colorTransformer->transformCollection($colors->all())
           ] );
        }

        //If normal route access, give them the view
        $this->data = array(
            'colors' => $colors,
            'title' => 'All Colors',
            'menu'	=> 'settings'
        );

		return View::make('colors.index', $this->data);
	}

	/**
	 * Show the form for creating a new color.
	 * GET /colors/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'New Color',
			'menu' 	=>	'settings' 
			);
		return View::make('colors.create', $this->data);
	}

	/**
	 * Store a newly created color in storage.
	 * POST /colors
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Color::$rules);
		if($validator->passes())
		{
			$color = new Color();
			$color->color_name = Input::get('color_name');
			
			if($color->save())
			{

			return Redirect::to('colors')
				->with('message', 'Success !! New Color created.');
			}

			
		}else
		return Redirect::to('colors/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified color.
	 * GET /colors/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $color = $this->colorRepository->getByID($id);

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$color)
            {
                return $this->respondNotFound("Color not found !!");
            }
            return $this->respond([
                'data' => $this->colorTransformer->transform($color)
            ] );
        }

        //If normal route access, give them the view

		if(!is_null($color))
        {
			$this->data = array(
				'color'  => $color,
				'title' => 'Color',
				'menu'	=>	'settings'
				);
		return View::make('colors.show', $this->data);
		}
		return Redirect::to('colors')
			->with('message', 'Color could not be found.');

	}

	/**
	 * Show the form for editing the specified color.
	 * GET /colors/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $color = $this->colorRepository->getByID($id);
        if(is_null($color))
		{
			return Redirect::to('colors')->with('message', 'Color not found. Please try another.' );
		}
		$this->data = array(
			'title' => 'Edit Color',
			'color' => $color,
			'menu'	=>	'settings'
			);

		return View::make('colors.edit', $this->data);
	}

	/**
	 * Update the specified color in storage.
	 * PUT /colors/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $color = $this->colorRepository->getByID($id);
        if(!is_null($color))
        {
			$rules = array(
				'color_name' => 'required|min:1|unique:colors,color_name,'.$id.',id,deleted_at,NULL'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$color->color_name = Input::get('color_name');
					
					if($color->update())
					{
					return Redirect::to('colors')
							->with('message', 'Success !! Color updated.');
					}
			}
			return Redirect::route('colors.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('colors')
			->with('message', 'Color not available. Please Retry.');
	}

	/**
	 * Remove the specified color from storage.
	 * DELETE /colors/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $color = $this->colorRepository->getByID($id);
		if(!is_null($color))
        {
		    //delete the color record
			$color->delete();
			return Redirect::to('colors')
				->with('message', 'Success !! Color has been deleted.');
		}
		return Redirect::to('colors')
			->with('message', 'Color not available. Please Retry.');
	}

}