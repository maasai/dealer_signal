<?php

use Dealer\Repositories\Feature\FeatureRepository;
use Dealer\Transformers\FeatureTransformer;

class FeaturesController extends ApiController {

    /**
     * @var featureTransformer
     */
    private $featureTransformer;

    /**
     * @var featureRepository
     */
    private $featureRepository;

    /**
     * We need both repository and transformer for out data
     * @param featureRepository $featureRepository
     * @param featureTransformer $featureTransformer
     */
    function __construct(FeatureRepository $featureRepository, FeatureTransformer $featureTransformer)
    {
        $this->featureTransformer = $featureTransformer;
        $this->featureRepository = $featureRepository;
    }

	/**
	 * Display a listing of the feature.
	 * GET /features
	 *
	 * @return Response
	 */
	public function index()
	{
        $features = $this->featureRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$features)
            {
                return $this->respondNotFound("Feature not found !!");
            }
            return $this->respondWithPagination($features,[
                'data' => $this->featureTransformer->transformCollection($features->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'features' => $features,
			'title' => 'All Features',
			'menu'	=> 'features'
			);

		return View::make('features.index', $this->data);
	}

	/**
	 * Show the form for creating a new feature.
	 * GET /features/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'New Feature',
			'menu' 	=>	'features' 
			);
		return View::make('features.create', $this->data);
	}

	/**
	 * Store a newly created feature in storage.
	 * POST /features
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Feature::$rules);
		if($validator->passes())
		{
			$feature = new Feature();
			$feature->feature_name = Input::get('feature_name');
			
			if($feature->save())
			{

			return Redirect::to('features')
				->with('message', 'Success !! New feature created.');
			}

			
		}else
		return Redirect::to('features/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified feature.
	 * GET /features/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $feature = $this->featureRepository->getByID($id);

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$feature)
            {
                return $this->respondNotFound("Feature not found !!");
            }
            return $this->respond([
                'data' => $this->featureTransformer->transform($feature)
            ] );
        }

        //If normal route access, give them the view;
		if(!is_null($feature)){
			$this->data = array(
				'feature'  => $feature,
				'title' => 'feature',
				'menu'	=>	'features'
				);
		return View::make('features.show', $this->data);
		}
		return Redirect::to('features')
			->with('message', 'Feature could not be found.');

	}

	/**
	 * Show the form for editing the specified feature.
	 * GET /features/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$feature = Feature::find($id);
		if(is_null($feature))
		{
			return Redirect::to('features')->with('message', 'Feature not found. Please try another.' );
		}
		$this->data = array(
			'title' => 'Edit Feature',
			'feature' => $feature,
			'menu'	=>	'features'
			);

		return View::make('features.edit', $this->data);
	}

	/**
	 * Update the specified feature in storage.
	 * PUT /features/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$feature = Feature::find($id);
		if(!is_null($feature)){
			$rules = array(
				'feature_name' => 'required|min:1|unique:features,feature_name,'.$id.',id,deleted_at,NULL'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$feature->feature_name = Input::get('feature_name');
					
					if($feature->update())
					{
					return Redirect::to('features')
							->with('message', 'Success !! Feature updated.');
					}
			}
			return Redirect::route('features.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('features')
			->with('message', 'feature not available. Please Retry.');
	}

	/**
	 * Remove the specified feature from storage.
	 * DELETE /features/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$feature = Feature::find($id);
		if(!is_null($feature)){
			
			//delete the feature record
			$feature->delete();
			return Redirect::to('features')
				->with('message', 'Success !! Feature has been deleted.');
		}
		return Redirect::to('features')
			->with('message', 'Feature not available. Please Retry.');
	}


}