<?php
use Dealer\Repositories\Make\MakeRepository;
use Dealer\Transformers\MakeTransformer;

class MakesController extends ApiController {

    /**
    * @var MakeTransformer
    */
    private $makeTransformer;

    /**
     * @var MakeRepository
     */
    private $makeRepository;

    /**
     * We need both repository and transformer for out data
     * @param MakeRepository $makeRepository
     * @param MakeTransformer $makeTransformer
     */
    function __construct(MakeRepository $makeRepository, MakeTransformer $makeTransformer)
    {
        $this->makeTransformer = $makeTransformer;
        $this->makeRepository = $makeRepository;
    }

	/**
	 * Display a listing of the resource.
	 * GET /makes
	 *
	 * @return Response
	 */
	public function index()
	{
	    $makes = $this->makeRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$makes)
            {
                return $this->respondNotFound("Vehicle make not found !!");
            }
            return $this->respondWithPagination($makes,[
                'data' => $this->makeTransformer->transformCollection($makes->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'makes' => $makes,
			'title' => 'All Vehicle Makes',
			'menu'	=> 'makes'
			);

		return View::make('makes.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /makes/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'New Vehicle Make',
			'menu' 	=>	'makes' 
			);
		return View::make('makes.create', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /makes
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Make::$rules);
		if($validator->passes())
		{
			$make = new Make();
			$make->make_name = Input::get('make_name');
			
			if($make->save())
			{

			return Redirect::to('makes')
				->with('message', 'Success !! New make created.');
			}

			
		}else
		return Redirect::to('makes/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 * GET /makes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $make = $this->makeRepository->getByID($id);
        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$make)
            {
                return $this->respondNotFound("Vehicle make not found !!");
            }
            return $this->respond([
                'data' => $this->makeTransformer->transform($make)
            ] );
        }

        //If normal route access, give them the view
        if(!is_null($make)){
			$this->data = array(
				'make'  => $make,
				'title' => 'Make',
				'menu'	=>	'makes'
				);
		return View::make('makes.show', $this->data);
		}
		return Redirect::to('makes')
			->with('message', 'Make could not be found.');

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /makes/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$make = Make::find($id);
		if(is_null($make))
		{
			return Redirect::to('makes')->with('message', 'Make not found. Please try another.' );
		}
		$this->$data = array(
			'title' => 'Edit Vehicle Make',
			'make' => $make,
			'menu'	=>	'makes'
			);

		return View::make('makes.edit', $this->$data);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /makes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$make = Make::find($id);
		if(!is_null($make)){
			$rules = array(
				'make_name' => 'required|min:1|unique:makes,make_name,'.$id.',id,deleted_at,NULL'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$make->make_name = Input::get('make_name');
					
					if($make->update())
					{
					return Redirect::to('makes')
							->with('message', 'Success !! Vehicle Make updated.');
					}
			}
			return Redirect::route('makes.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('makes')
			->with('message', 'Vehicle Make not available. Please Retry.');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /makes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$make = Make::find($id);
		if(!is_null($make)){
			
			//delete the make record
			$make->delete();
			return Redirect::to('makes')
				->with('message', 'Success !! Vehicle Make has been deleted.');
		}
		return Redirect::to('makes')
			->with('message', 'Vehicle Make not available. Please Retry.');
	}

}