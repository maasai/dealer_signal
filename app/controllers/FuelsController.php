<?php

use Dealer\Repositories\Fuel\FuelRepository;
use Dealer\Transformers\FuelTransformer;

class FuelsController extends ApiController {

    /**
     * @var fuelTransformer
     */
    private $fuelTransformer;

    /**
     * @var fuelRepository
     */
    private $fuelRepository;

    /**
     * We need both repository and transformer for out data
     * @param fuelRepository $fuelRepository
     * @param fuelTransformer $fuelTransformer
     */
    function __construct(FuelRepository $fuelRepository, FuelTransformer $fuelTransformer)
    {
        $this->fuelTransformer = $fuelTransformer;
        $this->fuelRepository = $fuelRepository;
    }

	/**
	 * Display a listing of the fuel.
	 * GET /fuels
	 *
	 * @return Response
	 */
	public function index()
	{
        $fuels = $this->fuelRepository->getAll();

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$fuels)
            {
                return $this->respondNotFound("Fuel not found !!");
            }
            return $this->respondWithPagination($fuels,[
                'data' => $this->fuelTransformer->transformCollection($fuels->all())
            ] );
        }

        //If normal route access, give them the view
		$this->data = array(
			'fuels' => $fuels,
			'title' => 'All Fuel Types',
			'menu'	=> 'settings'
			);

		return View::make('fuels.index', $this->data);
	}

	/**
	 * Show the form for creating a new fuel.
	 * GET /fuels/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data = array(
			'title' => 'New Fuel Type',
			'menu' 	=>	'settings' 
			);
		return View::make('fuels.create', $this->data);
	}

	/**
	 * Store a newly created fuel in storage.
	 * POST /fuels
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$validator = Validator::make($input, Fuel::$rules);
		if($validator->passes())
		{
			$fuel = new Fuel();
			$fuel->fuel_name = Input::get('fuel_name');
			
			if($fuel->save())
			{

			return Redirect::to('fuels')
				->with('message', 'Success !! New Fuel type created.');
			}

			
		}else
		return Redirect::to('fuels/create')
			->withErrors($validator)
			->withInput()
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified fuel.
	 * GET /fuels/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $fuel = $this->fuelRepository->getByID($id);

        //Respond with JSON if api route is requested
        if (Request::is('api/*'))
        {
            if (!$fuel)
            {
                return $this->respondNotFound("Fuel not found !!");
            }
            return $this->respond([
                'data' => $this->fuelTransformer->transform($fuel)
            ] );
        }

        //If normal route access, give them the view
		if(!is_null($fuel)){
			$this->data = array(
				'fuel'  => $fuel,
				'title' => 'Fuel Type',
				'menu'	=>	'settings'
				);
		return View::make('fuels.show', $this->data);
		}
		return Redirect::to('fuels')
			->with('message', 'Fuel type could not be found.');

	}

	/**
	 * Show the form for editing the specified fuel.
	 * GET /fuels/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$fuel = Fuel::find($id);
		if(is_null($fuel))
		{
			return Redirect::to('fuels')->with('message', 'Fuel type not found. Please try another.' );
		}
		$this->data = array(
			'title' => 'Edit Fuel type',
			'fuel' => $fuel,
			'menu'	=>	'settings'
			);

		return View::make('fuels.edit', $this->data);
	}

	/**
	 * Update the specified fuel in storage.
	 * PUT /fuels/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$fuel = Fuel::find($id);
		if(!is_null($fuel)){
			$rules = array(
				'fuel_name' => 'required|min:1|unique:fuels,fuel_name,'.$id.',id,deleted_at,NULL'
						);
		$validator = Validator::make(Input::all(), $rules);
			if($validator->passes())
			{
					$fuel->fuel_name = Input::get('fuel_name');
					
					if($fuel->update())
					{
					return Redirect::to('fuels')
							->with('message', 'Success !! Fuel type updated.');
					}
			}
			return Redirect::route('fuels.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('fuels')
			->with('message', 'Fuel type not available. Please Retry.');
	}

	/**
	 * Remove the specified fuel from storage.
	 * DELETE /fuels/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$fuel = Fuel::find($id);
		if(!is_null($fuel)){
			
			//delete the fuel record
			$fuel->delete();
			return Redirect::to('fuels')
				->with('message', 'Success !! Fuel type has been deleted.');
		}
		return Redirect::to('fuels')
			->with('message', 'Fuel type not available. Please Retry.');
	}


}